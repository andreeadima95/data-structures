class Node:

    def __init__(self, data):
        self.previous = None
        self.next = None
        self.data = data

    def set_next(self, next_node):
        self.next = next_node

    def set_prev(self, previous_node):
        self.previous = previous_node


class DoubleLinkedList:
    
    def __str__(self):
        string = '['
        node = self.first_node
        while node is not None:
            string += str(node.data) 
            if node.next:
                string += ', '
            node = node.next
        return string + ']'

    def __init__(self):
        self.first_node = None

    @classmethod
    def init_with_list_items(cls, initial_data):
        new_list = DoubleLinkedList()
        for item in initial_data:
            new_list.push_back(item)
        return new_list

    @property
    def size(self):
        size = 0
        node = self.first_node
        while node:
            size += 1
            node = node.next
        return size

    @property
    def empty(self):
        return self.first_node is None

    @property
    def front(self):
        if not self.first_node:
            return None
        return self.first_node.data

    @property
    def back(self):
        if not self.first_node:
            return None
        node = self.first_node
        while node.next is not None:
            node = node.next
        return node.data

    def value_at(self, index):
        current_index = 0
        node = self.first_node
        while current_index < index and node is not None:
            current_index += 1
            node = node.next
        return node.data

    def push_front(self, data):
        node = self.first_node
        self.first_node = Node(data)
        self.first_node.set_next(node)
        if node:
            node.set_prev(self.first_node)

    def pop_front(self):
        if not self.first_node:
            return None
        node = self.first_node
        self.first_node = node.next
        self.first_node.set_prev(None)
        return node.data

    def push_back(self, data):
        if not self.first_node:
            self.first_node = Node(data)
            return
        node = self.first_node
        while node.next is not None:
            node = node.next
        new_node = Node(data)
        node.set_next(new_node)
        new_node.set_prev(node)

    def pop_back(self):
        if not self.first_node:
            return None
        node = self.first_node
        while node.next is not None:
            node = node.next
        node.previous.set_next(None)
        return node

    def insert(self, index, data):
        if index == 0:
            self.push_front(data)
            return
        current_index = 0
        node = self.first_node
        while current_index < index - 1 and node is not None:
            current_index += 1
            node = node.next
        if node is None:
            return 
        new_node = Node(data)
        new_node.set_prev(node)
        new_node.set_next(node.next)
        if node.next:
            node.next.set_prev(new_node)
        node.set_next(new_node)

    def erase(self, index):
        current_index = 0
        node = self.first_node
        if index == 0:
            self.pop_front()
            return
        while current_index < index - 1 and node is not None:
            current_index += 1
            node = node.next
        if node is None or node.next is None:
            return
        if node.next.next:
            node.next.next.set_prev(node)
        node.set_next(node.next.next)

    def value_n_from_end(self, n):
        if n > self.size:
            return None
        node = self.first_node
        while node.next:
            node = node.next
        counter = 1
        while counter < n:
            node = node.previous
            counter += 1
        return node.data

    def reverse(self):
        size = self.size
        half = size / 2
        current = 0
        while current < half:
            first_value = self.value_at(current)
            first_position = current
            second_value = self.value_at(size - current - 1)
            second_position = size - current - 1

            self.erase(first_position)
            self.insert(first_position, second_value)

            self.erase(second_position)
            self.insert(second_position, first_value)

            current += 1

    def get_first_index(self, data):
        node = self.first_node
        counter = 0
        while node and node.data != data:
            node = node.next
            counter += 1
        return counter if counter < self.size else -1

    def remove_value(self, data):
        index = self.get_first_index(data)
        if index:
            self.erase(index)
