from printing import print_data, new_line
from structures.list import DoubleLinkedList

# Lists

print("-------------------------LISTS-------------------------")

linked_list = DoubleLinkedList()
print_data('List', linked_list)
print_data('Is empty', linked_list.empty)
new_line()

linked_list = DoubleLinkedList.init_with_list_items([1, 2, 3, 4, 5])

print_data('List', linked_list)
print_data('Size', linked_list.size)
print_data('Value on 3', linked_list.value_at(3))
new_line()

linked_list.push_front(4)
print_data('List after push front', linked_list)
print_data('Front value', linked_list.front)
new_line()

linked_list.pop_front()
print_data('List after pop front', linked_list)
print_data('Front value', linked_list.front)
new_line()

linked_list.push_back(7)
print_data('List after push back', linked_list)
print_data('Back value', linked_list.back)
new_line()

linked_list.pop_back()
print_data('List after pop back', linked_list)
print_data('Back value', linked_list.back)
new_line()

linked_list.erase(3)
print_data('List after erase 3', linked_list)
new_line()

linked_list.insert(2, 55)
print_data('List after insert on 2', linked_list)
new_line()


print_data('Value 3 from end', linked_list.value_n_from_end(3))
new_line()

linked_list.reverse()
print_data('Reversed list', linked_list)
new_line()

linked_list.remove_value(55)
print_data('Removed value 55', linked_list)
new_line()